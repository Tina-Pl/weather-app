# WEATHER APP
---
A simple weather application that provides current weather information (weather condition, temperature, wind speed and humidity) for the city you choose to search for.

## Built With
---
* HTML5
* CSS3
* SASS
* jQuery
* [Ionicons](https://ionicons.com/v2/) - Icons
* [OpenWeatherMap.org](https://openweathermap.org) - Weather API

