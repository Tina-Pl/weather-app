$(document).ready(function() {

    $('#searchBtn').on('click', function() {
        // Get input value
        var city = $('#city').val();

        //Check if input is empty or not
        //If not empty
        if(city != '') {
            $.ajax({
                url: "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&units=metric" +
                     "&APPID=17fe5e86133e421df334e2b44d0cdd91",
                type: "GET",
                dataType: "jsonp",
                success: function(data) {
                   //console.log(data);
                   // Call showData function
                   showData(data);
                   // Empty input
                   $('#city').val('');
                }
            });
        } else {
        // If empty
            // Get input variable
            var input = $('#city');

            // Add 'error' class to input and change placeholder text
            input.addClass('error').attr('placeholder', 'Field cannot be empty');

            // On focus, remove 'error' class and change placeholder text to original value
            input.on('focus', function(){
                input.removeClass('error').attr('placeholder', 'Enter City Name');
            });

        }

    });
});

function showData(data) {
    var cityName = data.name;
    var country = data.sys.country;
    var temp = Math.floor(data.main.temp);
    var wind = data.wind.speed;
    var humidity = data.main.humidity;
    var weather = data.weather[0].main;
    var icon = data.weather[0].icon;

    $('.current-weather').html(weather);
    $('.location').html(cityName + ', ' + country);
    $('.info').eq(0).html(temp + ' &deg;C');
    $('.info').eq(1).html(wind + ' m/s');
    $('.info').eq(2).html(humidity + ' %');
    $('.icon').attr('src', 'https://openweathermap.org/img/w/' + icon + '.png');
}